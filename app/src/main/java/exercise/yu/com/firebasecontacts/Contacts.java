package exercise.yu.com.firebasecontacts;

/**
 * Created by User on 8/5/2016.
 */
public class Contacts {
    private int telephoneNumber;
    private String friendName;

    public Contacts(int telephoneNumber, String friendName) {
        this.telephoneNumber = telephoneNumber;
        this.friendName = friendName;
    }

    public Contacts() {
        /*default, no arguments constructor*/
    }

    public int getTelephoneNumber() {
        return telephoneNumber;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setTelephoneNumber(int telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }
}
