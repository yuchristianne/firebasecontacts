package exercise.yu.com.firebasecontacts;

import java.util.HashMap;

/**
 * Created by User on 8/5/2016.
 */
public class DummyData {
    static int[] numbers = {123456, 654321, 198765, 567890,375291,983276,182503};
    static String[] names = {"Jack Jones", "Mike Malone", "Peter Pan"
            , "Rodney Roper", "Harry Horne", "Casey Clarke", "Bernie Bunting"};

    public static HashMap<String, Contacts> getDummyDataAsHashMap() {
        HashMap<String, Contacts> friendsHashMap = new HashMap<>();
        for (int i = 0; i < numbers.length; i++) {
            Contacts c = new Contacts();
            c.setTelephoneNumber(numbers[i]);
            c.setFriendName(names[i]);
            friendsHashMap.put(String.valueOf(i), c);
        }
        return friendsHashMap;
    }
}
